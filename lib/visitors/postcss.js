"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

// PostCss
var postcss = require('postcss');

var postcssJs = require('postcss-js');

var plugins = [require('postcss-epub')(), require('postcss-will-change-transition')(), require('postcss-will-change')(), require('postcss-gradient-transparency-fix')(), require('pixrem')(), require('postcss-clearfix')(), require('postcss-focus')(), require('postcss-color-rgba-fallback')(), require('postcss-font-normalize')(), require('autoprefixer')({
  browsers: ["last 2 version", "IE >= 11", "Safari >= 11"]
}), require('postcss-ordered-values')(), require("postcss-discard-comments")(), require("css-mqpacker")(), require("postcss-unique-selectors")(), require("postcss-discard-duplicates")()];

var postcssParse = function postcssParse(value) {
  var parseStyle = postcssJs.objectify(postcss.parse(value));
  return postcss(plugins).process(parseStyle, {
    parser: postcssJs,
    from: null
  }).css.replace(/\s+/ig, '');
};

var _default = function _default(t) {
  return function (path, state) {
    if (path.node.quasi) {
      var templateLiteral = path.node.quasi;
      var quasisLength = templateLiteral.quasis.length;

      for (var i = 0; i < quasisLength; i++) {
        var element = templateLiteral.quasis[i];

        if (element.value.raw) {
          element.value.raw = postcssParse(element.value.raw);
        }

        if (element.value.cooked) {
          element.value.cooked = postcssParse(element.value.cooked);
        }
      }
    }
  };
};

exports.default = _default;